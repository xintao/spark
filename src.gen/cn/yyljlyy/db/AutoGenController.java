package cn.yyljlyy.db;

import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class AutoGenController extends Controller {
	public void index() {
		String db_list = getPara("db_list");
		setAttr("dbPage", Db.query("show TABLES"));
		Page<Record> pager = Db.paginate(getParaToInt("pager.pageNumber", 1),getParaToInt("pager.pageSize", 20),
				"select COLUMN_NAME,DATA_TYPE,COLUMN_COMMENT", 
				"FROM information_schema.`COLUMNS` WHERE TABLE_NAME = ? and TABLE_SCHEMA='spark'",db_list);
		setAttr("db_list", db_list);
		setAttr("dbPages", pager);
		render("dbpage.html");
	}
	
	public void dblistpage() {
		String db_list = getPara("db_list");
		System.out.println(db_list);
		String packages = getPara("packages");
		setAttr("dbPage", Db.query("show TABLES"));
		Page<Record> pager = Db.paginate(getParaToInt("pager.pageNumber", 1),getParaToInt("pager.pageSize", 20),
				"select COLUMN_NAME,DATA_TYPE,COLUMN_COMMENT", 
				"FROM information_schema.`COLUMNS` WHERE TABLE_NAME = ? and TABLE_SCHEMA='spark'",db_list);
		System.out.println(pager);
		List<Record> list = pager.getList();
		for (Record record : list) {
			Object object = record.get("COLUMN_COMMENT");
			System.out.println(object.toString());
		}
		setAttr("packages", packages);
		setAttr("dbList", pager);
		render("dblistpage.html");
	}
}
